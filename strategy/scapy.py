from urllib import request
import time
import datetime
class scapy(object):
    def __init__(self):
        self.url1 = 'http://stock.gtimg.cn/data/index.php?appn=detail&action=data&c='
        self.url2 = ' http://qt.gtimg.cn/q='

    def get_opening_session(self, btSymbol):
        flag = False
        if btSymbol.split('.')[0][0] == '6':
            stockID = 'sh' + btSymbol.split('.')[0]
        else:
            stockID = 'sz' + btSymbol.split('.')[0]
        while flag == False:
            try:
                url = self.url1 + stockID + '&p=0'
                rq = request.Request(url)
                response = request.urlopen(rq, timeout=3)
                contents = response.read()
                opening_session = str(contents).split("|")[0].split('/')[2]
                flag = True
                return float(opening_session)
            except:
                print(btSymbol, '没有集合竞价价格数据,正在尝试重新查询')

    def get_upperlimit(self, btSymbol):
        flag = False
        if btSymbol.split('.')[0][0] == '6':
            stockID = 'sh' + btSymbol.split('.')[0]
        else:
            stockID = 'sz' + btSymbol.split('.')[0]
        while flag == False:
            try:
                url = self.url2 + stockID
                rq = request.Request(url)
                response = request.urlopen(rq, timeout=3)
                contents = response.read()
                price = str(contents).split("~")
                flag= True
                return float(price[47])
            except:
                print(btSymbol, '没有涨停价价格数据,正在尝试重新查询')

    def get_opening_amount(self, btSymbol):
        flag = False
        if btSymbol.split('.')[0][0] == '6':
            stockID = 'sh' + btSymbol.split('.')[0]
        else:
            stockID = 'sz' + btSymbol.split('.')[0]
        while flag == False:
            try:
                url = self.url1 + stockID + '&p=0'
                rq = request.Request(url)
                response = request.urlopen(rq, timeout=3)
                contents = response.read()
                opening_session = str(contents).split("|")[0].split('/')[4]
                flag = True
                return float(opening_session)
            except:
                print(btSymbol, '没有集合竞价成交量数据,正在尝试重新查询')

if __name__ == '__main__':
    pass