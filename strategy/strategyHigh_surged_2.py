# encoding: UTF-8

"""
宝澜策略1(高位股)
策略必须定义 STRATEGY_NAME 且全局唯一
策略回调函数 必须满足 on_init/start/stop 或 handle_tick/bar/order/trade 等格式
该策略发出买单总额未设限制，具体的限制经过验证在IMS客户端具有该功能，故不重复设置
"""

from __future__ import division
import win_unicode_console
import time
import datetime
import pandas as pd

# 一些常量，如 Direction, Offset 等
from bigtrader.ctaStrategy.strategy.scapy import *
from bigtrader.ctaStrategy.strategy.sina_get_all import *
from bigtrader.btConstant import Direction, Offset, STRATEGY_ASSET_MULTI, RunMode, OrderStatus

from bigtrader.btObject import (BtTickData, BtBarData,
                                BtOrderData, BtTradeData)
from bigtrader.ctaStrategy.ctaStrategyEx import CTAStrategyEx, run_strategy
from bigtrader.ctaStrategy.barGenerator import BarGenerator
from bigtrader.ctaStrategy.barDataSeries import BarDataSeries
#解决Terminalprint()报错问题
win_unicode_console.enable()

#初始化腾讯端口爬虫
stock_scapy = scapy()

#初始化新浪数据接口
allstocks_info = Run_Marketall(0)

# 策略名字（必须），全局唯一，且必须与配置文件中的策略名一致
STRATEGY_NAME = "Highsurged_2"

# 策略作者（可选）
STRATEGY_AUTHOR = "BigQuant"

# 策略参数名字（可选），用于在UI上设置参数并传入给策略
STRATEGY_PARAM_LABELS = {
                         "period" : "交易周期",
                         "flag": "标志",
                         "instrument": "代码",
                         "increase_amount":"涨幅",#买入涨幅达到X%
                         "cash_perbuy":"下单单层金额",#一层仓位金额
                         "amount_perbuy":"下单层数",#购买逻辑所购买同一支股票的总量限制，最多购买X层
                         "remaining_amount":"委卖剩余手数",#涨停价委托卖单剩余X手时，买入
                         "upperlimit_amount":"单日买入股票数量上限",#单日所购买股票数量上限
                         "sell_time":"卖出检测时间点",#没有在涨停板上卖出的检测时间点
                         "stoploss_percentage_a":"跌幅百分比a",#清仓止损跌幅百分比
                         "stoploss_percentage_b":"跌幅百分比b",#清仓止损跌幅百分比
                         "stoploss_percentage_minute": "分时(1分钟)跌幅百分比",  # 分时一分钟跌幅百分比
                         "sell_delay":"卖出延时",#1分钟内下跌超过A%，Xs后卖出
                         "break_sell_time":"X点至X点破过板卖出监测时间点",#10点15分在涨停板上， 那么从10点15分之后开始 如果出现炸板则在XX点卖出， XX点默认为14:56分
                         "buypoint_limitup":"涨停买点选择",#开盘涨停买点选择
                         "buypoint_nolimitup":"未涨停买点选择",#开盘未涨停买点选择
                         "sellpoint":"卖点选择",#盘中卖点选择
                         "preamount_limit":"昨日成交量额度限制",#小于该额度的股票会被过滤
                         "amount_limit":"购买个股持仓+委托单总金额限制",#严格小于该金额
                         "market_increase_amount": "市场涨状态个股数量限制"  # 小于等于该数量则继续执行本策略
                         }

# 策略类型（该策略交易多个品种，必须指定该类型，该值为1）
STRATEGY_TYPE = STRATEGY_ASSET_MULTI

def parse_tdx_universe_content(content):
    """解析通达信股票池内容"""
    symbols = []
    lines = content.strip().split('\n')
    for line in lines:
        line = line.strip()
        if not line or len(line) < 6 or len(line) > 7:
            continue

        if line[0] == '0':
            symbols.append("%s.SZSE" % line[1:])
        else:
            symbols.append("%s.SSE" % line[1:])
    return symbols

#----------------------------------------------------------------------
# 策略回调函数(实现策略逻辑)
#----------------------------------------------------------------------
def on_init(context=CTAStrategyEx(None,None)):
    """初始化策略"""
    context.write_log('{} on_init()'.format(STRATEGY_NAME))
    context.period = context.get_conf_param('period', '1m')

    '''
    下列参数为用户可配置项，若欲修改,请仔细阅读注解并慎重进行配置
    '''
    #设置买入涨幅（买点:涨幅达到x%的时候 按涨停价买入一层仓位）
    context.increase_amount = context.get_conf_param('increase_amount', 0.09)
    #设置每层仓位资金金额(用于购买股票的每层资金)
    context.cash_perbuy = context.get_conf_param('cash_perbuy', 2000)
    #设置购买逻辑所购买同一支股票的层数上限(相同股票所购买层数上限)
    context.amount_perbuy =  context.get_conf_param('amount_perbuy', 2)
    #设置委卖剩余手数(买点:涨停价委托卖单只剩x手时买入)
    context.remaining_amount = context.get_conf_param('remaining_amount', 2000)
    #设置单日买入不同股票数量上限(用于购买不同股票的数量上限)
    context.upperlimit_amount = context.get_conf_param('upperlimit_amount', 2)
    #设置卖出检测时间点(卖点:第二天x点的时候是否在涨停板上)
    context.sell_time = context.get_conf_param('sell_time', 1015)
    #设置跌幅百分比(卖点:盘中任何时候如果价格达到x%)
    context.stoploss_percentage_a = context.get_conf_param('stoploss_percentage_a', -0.09)
    #设置跌幅百分比(卖点:如果A点的时候价格在涨停板上，A点到收盘的过程中如果价格达到当日x%)
    context.stoploss_percentage_b = context.get_conf_param('stoploss_percentage_b', 0.01)
    #设置跌幅百分比(卖点:分时从最高点1分钟内下跌超过x%，As后卖)
    context.stoploss_percentage_minute = context.get_conf_param('stoploss_percentage_minute', -0.09)
    #设置卖出延时(卖点:分时从最高点1分钟内下跌超过A%，xs后卖)
    context.sell_delay = context.get_conf_param('sell_delay', 10)
    #设置X点至X点破板卖出时间点(卖点:10点15分在涨停板上， 那么从10点15分之后开始 如果出现炸板则在XX点卖出， XX点默认为14:56分)
    context.break_sell_time = context.get_conf_param('break_sell_time', 1456)
    #设置将要选择的涨停买点(a,b)
    context.buypoint_limitup = context.get_conf_param('buypoint_limitup',['a','b'])
    #设置将要选择的非涨停买点(c,d,e)
    context.buypoint_nolimitup = context.get_conf_param('buypoint_nolimitup', ['c','d','e'])
    #设置将要选择的卖点(f,g,h,i,j)
    context.sellpoint = context.get_conf_param('sellpoint', ['f','g','h','i','j','k','l'])
    # 设置个股昨日成交量过滤限制
    context.preamount_limit = context.get_conf_param('preamount_limit', 75000000)
    # 设置个股持仓+委托单总金额限制
    context.amount_limit = context.get_conf_param('amount_limit', 200000)
    # 设置市场涨状态个股数量限制
    context.market_increase_amount = context.get_conf_param('market_increase_amount', 2500)

    '''
    下列与用户配置无关，请勿擅自修改
    '''
    # 初始化买点单次执行信号
    context.buypoint_a_list = []
    context.buypoint_b_list = []
    context.buypoint_c_list = []
    context.buypoint_d_list = []
    context.buypoint_e_list = []
    context.sellpoint_f_list = []
    context.sellpoint_g_list = []
    context.sellpoint_h_list = []
    context.sellpoint_i_list = []
    context.sellpoint_j_list = []
    context.sellpoint_k_list = []
    context.sellpoint_l_list = []
    # 初始化存放市价
    context.lastprice = {}
    #初始化10秒延时购买股票列表
    context.order_dict ={}
    #初始化计时器
    context.time_counter_dict = {}
    #初始化存放股票池所有股票9：25集合竞价是否为当日涨停状态
    context.surged_sign = {}
    #初始化存放10点价位在涨停板上的股票代码
    context.record = []
    #初始化存放复盘9%购买方式信息(涨幅达到9%买入一层仓，有可能个股最终没有达到涨停板价格)
    context.buy_set = set()
    #初始化存放一个tick数据判断是否执行卖空操作信号
    context.sign_ask = False
    # 初始化记录账户所持股票可卖数量信息
    context.pos_avail = {}
    # 初始化动态序列默认大小为20（即1分钟20个tick数据）
    context.init_dynamic_series(20)
    # 初始化单次执行信号
    context.isfirst = True
    # 初始化卖单撤单信息
    context.cancel_sell_orders = {}
    # 初始化存放昨日成交量
    context.prevolume = {}
    # 初始化存放昨日成交额度
    context.preamount = {}
    #初始化存放破板固定X点X分卖出票
    context.break_sign = {}
    # 初始化暂停交易信号值
    context.stoptrade_sign = False

    # 从文件获取股票池
    filepath = context.get_conf_param("universeFile", "")
    content = context.get_file_content(filepath)
    if not content:
        print("%s on_init no universe content from universeFile:%s" % (STRATEGY_NAME, filepath), end='\n')
        return

    symbols = parse_tdx_universe_content(content)
    context.set_universe(symbols)
    print(">>>>>>>>>>>>>> %s symbols: %s" %(STRATEGY_NAME, symbols), end='\n')

def on_start(context=CTAStrategyEx(None,None)):

    """启动策略"""
    context.write_log('{} on_start()'.format(STRATEGY_NAME))
    flag = False
    #连接交易账户
    ta = context.get_trading_account()
    #获取代码池
    context.today_symbols = context.get_universe()
    # 记录账户所持仓股票代码信息，包含已购买后卖出持股0个股信息
    context.symbols_holds = [s.btSymbol for s in context.get_account_position(None)]
    # 将已有持仓股票信息和今日待订阅股票信息加入今日订阅池
    all_symbols = list(set(context.today_symbols + context.symbols_holds))
    # 记录开盘涨停状态
    context.surged_sign = {k:None for k in all_symbols}
    # 记录账户所持股票可卖数量信息
    context.pos_avail = {k.btSymbol: k.long_avail for k in context.get_account_position(None)}
    #记录上笔市价
    context.lastprice = {k:0.0 for k in all_symbols}
    #分别打印今日股票池与持仓股票信息
    print("%s on_start trading account:%s" % (STRATEGY_NAME, ta), end='\n')
    print("%s on_start today_symbols:%s" % (STRATEGY_NAME, context.today_symbols), end='\n')
    print("%s on_start holding symbols:%s" %(STRATEGY_NAME, context.symbols_holds), end='\n')
    print("%s all subscribe symbols(today_symbols + holding symbols):%s" %(STRATEGY_NAME, all_symbols), end='\n')

    # 加载昨日日线行情,用于竞价卖出
    if len(context.today_symbols) == 0:
        print("%s 今日股池为空,未加载昨日行情!" % STRATEGY_NAME, end='\n')
        context.write_log("{} 今日股池为空,未加载昨日行情!".format(STRATEGY_NAME))
    else:
        print("%s 加载昨日股票成交量行情: %s " % (STRATEGY_NAME, context.today_symbols))
        context.write_log("{} 加载昨日股票成交量行情: {} ".format(STRATEGY_NAME, context.today_symbols))
        while flag == False :
            # 初始化昨日股票行情
            try:
                df2 = context.get_bars_from_bigquant(context.today_symbols, 1)
                df2['instrument'] = df2['instrument'].apply(lambda x: x.replace("SHA", "SSE").replace("SZA", "SZSE"))
                context.preamount = {k: p for k, p in zip(df2['instrument'].values, df2['amount'].values)}
                print("%s 统计昨日成交额度成功: %s" % (STRATEGY_NAME, context.preamount), end='\n')
                context.write_log("{} 统计昨日成交额度成功: {}".format(STRATEGY_NAME, context.preamount))
                flag=True
            except:
                print("%s 加载昨日个股成交信息失败,正在重新查询..." % STRATEGY_NAME)
                context.write_log("{} 加载昨日个股成交信息失败,正在重新查询...".format(STRATEGY_NAME))
                context.preamount = {}
                flag=False

    # 订阅行情，包括已定股票池与账户已持有股票池
    context.subscribe(all_symbols)
    print("%s 策略开始运行......" % STRATEGY_NAME, end='\n')

def on_stop(context=CTAStrategyEx(None,None)):
    """停止策略"""
    context.write_log('{} 盘中涨幅超过9%的股票尾盘未涨停的股票列表:{}'.format(STRATEGY_NAME, context.buy_set))
    context.write_log("策略1买点1.1.1 当日买入股票 {}".format(context.buypoint_c_list))
    context.write_log("策略1买点1.1.2 当日买入股票 {}".format(context.buypoint_d_list))
    context.write_log("策略1买点1.1.3 当日买入股票 {}".format(context.buypoint_e_list))
    context.write_log("策略1买点2.1.1 当日买入股票 {}".format(context.buypoint_a_list))
    context.write_log("策略1买点2.1.2 当日买入股票 {}".format(context.buypoint_b_list))
    context.write_log("策略1卖点1.1 当日买入股票 {}".format(context.sellpoint_f_list))
    context.write_log("策略1卖点1.2 当日买入股票 {}".format(context.sellpoint_g_list))
    context.write_log("策略1卖点1.3 当日买入股票 {}".format(context.sellpoint_h_list))
    context.write_log("策略1卖点1.4 当日买入股票 {}".format(context.sellpoint_i_list))
    context.write_log("策略1卖点1.5 当日买入股票 {}".format(context.sellpoint_j_list))
    context.write_log("策略1卖点1.6 当日买入股票 {}".format(context.sellpoint_k_list))
    context.write_log("策略1卖点1.7 当日买入股票 {}".format(context.sellpoint_l_list))
    context.write_log('{} on_stop()'.format(STRATEGY_NAME))
    print("%s 策略终止运行......" % STRATEGY_NAME, end='\n')

def on_update(context=CTAStrategyEx(None,None), newSetting=None):
    """更新策略"""
    print("%s on_update:%s" % (STRATEGY_NAME, newSetting), end='\n')

def on_timer(context=CTAStrategyEx(None, None), data=None):
    """定时器事件(每秒回调一次)"""
    if context.order_dict != {}:
        new_order_dict = context.order_dict.copy()
        for btSymbol in new_order_dict.keys():
            if context.time_counter_dict[btSymbol] < context.sell_delay:
                context.time_counter_dict[btSymbol] += 1
            if context.time_counter_dict[btSymbol] == context.sell_delay:
                context.order(btSymbol, -1 * context.order_dict[btSymbol][0], round(context.order_dict[btSymbol][1], 2))
                print("%s %s 一分钟内跌幅达到 %s，延时%s s卖出" % (STRATEGY_NAME, btSymbol, context.stoploss_percentage_minute, context.sell_delay), end='\n')
                context.write_log("{} {} 一分钟内跌幅达到 {} ，延时{} s卖出".format(STRATEGY_NAME, btSymbol, context.stoploss_percentage_minute, context.sell_delay))
                del (context.order_dict[btSymbol])
                del (context.time_counter_dict[btSymbol])

#----------------------------------------------------------------------
# 实时交易数据通知
#----------------------------------------------------------------------
def handle_tick(context=CTAStrategyEx(None, None), tick=BtTickData()):
    """收到行情TICK推送 tick:BtTickData """
    if context.stoptrade_sign == True:
        return

    #该笔tick行情待判断买入额度初始化
    amount_bid = 0
    #时间转格式
    time_now = int(tick.datetime.hour) * 100 + int(tick.datetime.minute)
    # 记录账户所持股票可卖数量信息
    context.pos_avail = {k.btSymbol: k.long_avail for k in context.get_account_position(None)}
    #追卖单
    order = context.get_open_orders(tick.btSymbol)
    #FIXME pos_avail?
    if order != [] and tick.btSymbol in context.pos_avail:
        unfinished_sellorder = [k for k in order if k.direction == Direction.SHORT and k.tradedVolume != k.totalVolume]
        if len(unfinished_sellorder) > 0 and context.pos_avail[tick.btSymbol] == 0:
            unfinished_orderprice = unfinished_sellorder[0].price
            context.write_log("{} {} 未完成卖单 {}".format(STRATEGY_NAME, tick.time, unfinished_sellorder))
            if tick.lastPrice < unfinished_orderprice and tick.lastPrice != 0:
                print("%s %s 当前价格 %s 低于上笔委托卖单价格 %s，撤单降低当前价格的两个点重上卖单!" % (STRATEGY_NAME, tick.btSymbol, tick.lastPrice, unfinished_orderprice), end='\n')
                context.write_log("{} {} 当前价格 {} 低于上笔委托卖单价格 {} 撤单降低当前价格的两个点重上卖单!".format(STRATEGY_NAME, tick.time, tick.lastPrice,unfinished_orderprice))
                unsell_amount = unfinished_sellorder[0].totalVolume -  unfinished_sellorder[0].tradedVolume
                context.cancel_order(unfinished_sellorder[0])
                context.cancel_sell_orders[tick.btSymbol] = [unsell_amount, max(tick.lastPrice * 0.98, tick.lowerLimit)]

    #查看9:25以前数据通道是否正常
    if time_now < 925:
        print("%s handle_tick %s " %(STRATEGY_NAME, tick.log_str()), end='\n')

    #9：25集合竞价
    if time_now == 925:
        # 从今日股池里过滤集合竞价成交量小于昨日成交量的个股
        if tick.btSymbol in context.preamount.keys() and tick.btSymbol in context.today_symbols:
            if context.preamount[tick.btSymbol] <= context.preamount_limit:
                print("%s 过滤今日个股( 昨日成交量小于%s ): %s" % (STRATEGY_NAME, context.preamount_limit, tick.btSymbol))
                context.today_symbols.remove(tick.btSymbol)
                context.write_log("{} {} 过滤今日个股( 昨日成交量小于 {} ) {}".format(STRATEGY_NAME, tick.time, context.preamount_limit, tick.btSymbol))
        if tick.btSymbol in context.symbols_holds:
            context.lastprice[tick.btSymbol] = tick.lastPrice
        # 记录是否股票开盘是否涨停
        if tick.lastPrice == tick.upperLimit:
            context.surged_sign[tick.btSymbol] = True
            print('%s %s开盘涨停!' % (STRATEGY_NAME, tick.btSymbol), end='\n')
            context.write_log('{} {} 开盘是否涨停:  {} 集合竞价价格: {} 涨停价格 {}'.format(STRATEGY_NAME, tick.btSymbol, context.surged_sign[tick.btSymbol], tick.lastPrice, tick.upperLimit))
        elif tick.lastPrice < tick.upperLimit:
            context.surged_sign[tick.btSymbol] = False
            print('%s %s开盘未涨停!'%(STRATEGY_NAME, tick.btSymbol), end='\n')
            context.write_log('{} {} 开盘是否涨停:  {} 集合竞价价格: {} 涨停价格 {}'.format(STRATEGY_NAME, tick.btSymbol, context.surged_sign[tick.btSymbol], tick.lastPrice, tick.upperLimit))

    # 时间过了9点25,开始统计集合竞价为涨状态个股数量
    if 930 > time_now > 925:
        increase_amount = allstocks_info.run()
        if increase_amount > context.market_increase_amount:
            # 如果当前市场监测符合要求股票数量大于设置的阈值，则退出不再执行该策略
            # time.sleep(99999999999999999999999999999999999999)
            context.stoptrade_sign = True

    # 时间过了9点30分 :
    if time_now >= 930:
        if context.isfirst == True:    
            if list(context.surged_sign.values())[0] == None:
                print("%s 断点重连开盘行情..."%(STRATEGY_NAME), end='\n')
                context.write_log("{} 断点重连开盘行情...".format(STRATEGY_NAME))
                for btSymbol in context.surged_sign.keys():
                    context.surged_sign[btSymbol] = (stock_scapy.get_opening_session(btSymbol) == stock_scapy.get_upperlimit(btSymbol))
                    print("%s 断点查询开盘个股涨停状态: %s %s"%(STRATEGY_NAME, btSymbol, context.surged_sign[btSymbol]))
                    context.write_log("{} 断点查询开盘个股涨停状态: {} {}".format(STRATEGY_NAME, btSymbol, context.surged_sign[btSymbol]))
                for btSymbol in context.today_symbols.copy():
                    if btSymbol in context.preamount.keys():
                        if context.preamount[btSymbol] <= context.preamount_limit:
                            print("%s 过滤今日个股( 昨日成交量小于%s ): %s" % (STRATEGY_NAME, context.preamount_limit, btSymbol))
                            context.today_symbols.remove(btSymbol)
                            context.write_log("{} {} 过滤今日个股( 昨日成交量小于 {} ) {}".format(STRATEGY_NAME, datetime.datetime.now().time(), context.preamount_limit, btSymbol))
            print("今日被过滤后( 集合竞价成交量小于昨日%s )剩余股票: %s" % (context.preamount_limit, context.today_symbols))
            context.write_log("{} {} 今日被过滤后( 集合竞价成交量小于昨日{} )剩余股票(今日执行策略所用股票): {}".format(STRATEGY_NAME, datetime.datetime.now().time(), context.preamount_limit, context.today_symbols))
            context.write_log("{} {} 执行策略所有个股涨停信号 {}".format(STRATEGY_NAME, datetime.datetime.now().time(), context.surged_sign))
            context.write_log("{} 执行策略所用买点 {} {}".format(STRATEGY_NAME, context.buypoint_limitup, context.buypoint_nolimitup))
            context.write_log("{} 执行策略所用卖点 {} ".format(STRATEGY_NAME, context.sellpoint))
            context.isfirst = False
        context.append_to_dynamic_series(tick)
        bar = context.get_bar_from_dynamic_series(tick.btSymbol)
        #当前票在今日选定购买股票池里
        if tick.btSymbol in context.today_symbols:
             #一字板开盘
            if context.surged_sign[tick.btSymbol] == True:
                # 行情破板
                if tick.lowPrice != tick.upperLimit:
                    stockbuy_num = len([k.btSymbol for k in context.get_account_position(None) if k.btSymbol in context.today_symbols and k.long_avail == 0 and k.long_pos > 0])
                    # 涨幅到达涨停板的价格
                    if tick.lastPrice == tick.upperLimit and context.lastprice[tick.btSymbol] != tick.upperLimit and 'a' in context.buypoint_limitup :
                        # 今日已买股票数量未超过2
                        if stockbuy_num < context.upperlimit_amount and tick.btSymbol not in context.buypoint_a_list:
                            my_account = context.get_trading_account()
                            cash_for_buy = min(context.cash_perbuy, my_account.account.available)
                            amount_bid += int(cash_for_buy / tick.upperLimit / 100) * 100
                            print("%s handle_tick %s" % (STRATEGY_NAME, tick.log_str()), end='\n')
                            print("%s %s %s 一字板开盘破板又上涨停板，预计买入 %s" % (STRATEGY_NAME, tick.time, tick.btSymbol, str(int(cash_for_buy / tick.upperLimit / 100) * 100)), end='\n')
                            context.buypoint_a_list.append(tick.btSymbol)
                            context.write_log('{} {} {} 一字板开盘破板又上涨停板，预计买入{} 最低价格 {} 最高价格 {}'.format(STRATEGY_NAME, tick.time, tick.btSymbol, str(int(cash_for_buy / tick.upperLimit / 100) * 100), str(tick.lowPrice), str(tick.highPrice)))

                    # 卖一是涨停价，并且卖一委托量小于2000手，以涨停价下单一层
                    if tick.askPrice1 == tick.upperLimit and tick.askVolume1 < context.remaining_amount * 100 and context.lastprice[tick.btSymbol] != tick.upperLimit and 'b' in context.buypoint_limitup :
                        # 今日已买股票数量未超过2
                        if stockbuy_num < context.upperlimit_amount and tick.btSymbol not in context.buypoint_b_list:
                            my_account = context.get_trading_account()
                            cash_for_buy = min(context.cash_perbuy, my_account.account.available)
                            amount_bid += int(cash_for_buy / tick.upperLimit / 100) * 100
                            print("%s handle_tick %s" %(STRATEGY_NAME, tick.log_str()), end='\n')
                            print("%s %s %s一字板开盘卖一涨停且卖一委托量小于%s手,预计买入 %s " % (STRATEGY_NAME, tick.time, tick.btSymbol, context.remaining_amount, int(cash_for_buy / tick.upperLimit / 100) * 100), end='\n')
                            context.buypoint_b_list.append(tick.btSymbol)
                            context.write_log('{} {} {} 一字板开盘卖一涨停且卖一委托量小于{}手,预计买入{}'.format(STRATEGY_NAME, tick.time, tick.btSymbol, context.remaining_amount, str(int(cash_for_buy / tick.upperLimit / 100) * 100)))
            #非一字板开盘
            elif context.surged_sign[tick.btSymbol] == False:
                stockbuy_num = len([k.btSymbol for k in context.get_account_position(None) if k.btSymbol in context.today_symbols and k.long_avail == 0 and k.long_pos > 0])
                # 涨幅达到百分之9
                if tick.lastPrice / tick.preClosePrice - 1 >= context.increase_amount and context.lastprice[tick.btSymbol] < tick.lastPrice and tick.lastPrice != tick.upperLimit and 'c' in context.buypoint_nolimitup:
                    # 今日已买股票数量未超过2
                    if stockbuy_num < context.upperlimit_amount and tick.btSymbol not in context.buypoint_c_list:
                        my_account = context.get_trading_account()
                        cash_for_buy = min(context.cash_perbuy, my_account.account.available)
                        amount_bid += int(cash_for_buy / tick.upperLimit / 100) * 100
                        print("%s handle_tick %s" %(STRATEGY_NAME, tick.log_str()), end='\n')
                        print("%s %s %s 非一字板涨幅超过 %s，预计买入 %s" % (STRATEGY_NAME, tick.time, tick.btSymbol, context.increase_amount, str(int(cash_for_buy / tick.upperLimit / 100) * 100)), end='\n')
                        context.buypoint_c_list.append(tick.btSymbol)
                        context.write_log('{} {} {} 非一字板涨幅超过{}，预计买入 {}'.format(STRATEGY_NAME, tick.time, tick.btSymbol, context.increase_amount, str(int(cash_for_buy / tick.upperLimit / 100) * 100)))
                        # 记录该方式买入个股
                        context.buy_set.add(tick.btSymbol)

                # 涨幅到达涨停板的价格
                if tick.lastPrice == tick.upperLimit and context.lastprice[tick.btSymbol] != tick.upperLimit and 'd' in context.buypoint_nolimitup:
                    # 今日已买股票数量未超过2
                    if stockbuy_num < context.upperlimit_amount and tick.btSymbol not in context.buypoint_d_list:
                        my_account = context.get_trading_account()
                        cash_for_buy = min(context.cash_perbuy, my_account.account.available)
                        amount_bid += int(cash_for_buy / tick.upperLimit / 100) * 100
                        print("%s handle_tick %s" %(STRATEGY_NAME, tick.log_str()), end='\n')
                        print("%s %s %s 非一字板上涨停板，预计买入 %s" % (STRATEGY_NAME, tick.time, tick.btSymbol, str(int(cash_for_buy / tick.upperLimit / 100) * 100)), end='\n')
                        context.buypoint_d_list.append(tick.btSymbol)
                        context.write_log('{} {} {} 非一字板上涨停板，预计买入 {}'.format(STRATEGY_NAME, tick.time, tick.btSymbol, str(int(cash_for_buy / tick.upperLimit / 100) * 100)))

                # 卖一是涨停价,并且卖一委托量小于2000手
                if tick.askPrice1 == tick.upperLimit and tick.askVolume1 < context.remaining_amount * 100 and context.lastprice[tick.btSymbol] != tick.upperLimit and 'e' in context.buypoint_nolimitup:
                    # 今日已买股票数量未超过2
                    if stockbuy_num < context.upperlimit_amount and tick.btSymbol not in context.buypoint_e_list:
                        my_account = context.get_trading_account()
                        cash_for_buy = min(context.cash_perbuy, my_account.account.available)
                        amount_bid += int(cash_for_buy / tick.upperLimit / 100) * 100
                        print("%s handle_tick %s" %(STRATEGY_NAME, tick.log_str()), end='\n')
                        print("%s %s %s 非一字板卖一涨停且卖一委托量小于%s手,预计买入 %s " % (STRATEGY_NAME, tick.time, tick.btSymbol, context.remaining_amount, int(cash_for_buy / tick.upperLimit / 100) * 100), end='\n')
                        context.buypoint_e_list.append(tick.btSymbol)
                        context.write_log('{} {} {} 非一字板卖一涨停且卖一委托量小于{}手,预计买入 {}'.format(STRATEGY_NAME, tick.time, tick.btSymbol, context.remaining_amount, str(int(cash_for_buy / tick.upperLimit / 100) * 100)))

        # 盘中任何时候达到-9%且可卖数量>0,卖出
        if tick.btSymbol in context.pos_avail and tick.lastPrice / tick.preClosePrice -1  <= context.stoploss_percentage_a and 'g' in context.sellpoint:
            if context.pos_avail[tick.btSymbol] > 0 and tick.btSymbol not in context.sellpoint_g_list:
                context.sign_ask = True
                context.sellpoint_g_list.append(tick.btSymbol)
                print("%s handle_tick %s" %(STRATEGY_NAME, tick.log_str()), end='\n')
                print("%s %s %s盘中跌幅达到%s ,卖出 %s" % (STRATEGY_NAME, tick.time,tick.btSymbol, context.stoploss_percentage_a, -1 * context.pos_avail[tick.btSymbol]), end='\n')
                context.write_log('{} {} {} 盘中跌幅达到{}，卖出 {}'.format(STRATEGY_NAME, tick.time, tick.btSymbol, context.stoploss_percentage_a, str(-1 * context.pos_avail[tick.btSymbol])))

        #盘中一分钟跌幅达到-9% TODO 时间序列跌在涨前
        if tick.btSymbol in context.pos_avail and bar.close / bar.high - 1 < context.stoploss_percentage_minute and 'j' in context.sellpoint:
            # 延时10s卖出当前所持tick
            if bar.btSymbol not in context.time_counter_dict.keys() and tick.btSymbol not in context.sellpoint_j_list:
                context.sellpoint_j_list.append(tick.btSymbol)
                context.time_counter_dict[bar.btSymbol] = 0
                context.order_dict[bar.btSymbol] = [context.pos_avail[bar.btSymbol], max(bar.low * 0.98, tick.lowerLimit)]
                context.write_log("{} {} {}开始 {} s计时，准备卖出".format(STRATEGY_NAME, tick.time, tick.btSymbol, context.sell_delay))

    #记录该点相关信息
    if time_now == context.sell_time:
        context.write_log("sell_time {}".format(context.sell_time))
        context.write_log("pos_avail {}".format(context.pos_avail))
        context.write_log("{} pos_avail {}".format(tick.btSymbol, context.sell_time))
        context.write_log("{} lastPrice {}".format(tick.btSymbol, tick.lastPrice))
        context.write_log("{} upperLimit {}".format(tick.btSymbol, tick.upperLimit))

    #时间到了XX:XX
    if time_now == context.sell_time:
        if tick.btSymbol in context.pos_avail:
            #如果账户持有股票可卖数量大于0，卖出
            if context.pos_avail[tick.btSymbol]>0 and tick.btSymbol not in context.sellpoint_f_list:
                #没在涨停板上(拆分依赖性)
                if tick.lastPrice < tick.upperLimit and'f' in context.sellpoint:
                    context.sign_ask = True
                    context.sellpoint_f_list.append(tick.btSymbol)
                    print("%s handle_tick %s" %(STRATEGY_NAME, tick.log_str()), end='\n')
                    print("%s %s 未在涨停板上，卖出 %s" %(STRATEGY_NAME, tick.btSymbol, tick.time), end='\n')
                    context.write_log('{} {} {}未在涨停板上，卖出 {}'.format(STRATEGY_NAME, tick.btSymbol, tick.time, str(-1 * context.pos_avail[tick.btSymbol])))
                #在涨停板上
                elif tick.lastPrice == tick.upperLimit:
                    if tick.btSymbol not in context.record:
                        context.record.append(tick.btSymbol)

    #X点的时候价格在涨停板上，X点到收盘的过程中如果价格下跌到涨幅x%，则卖出
    if time_now > context.sell_time:
        if tick.btSymbol in context.pos_avail:
            #该股票X点在涨停板上，且该股票可卖数量>0且未执行过该卖点
            if tick.btSymbol in context.record and context.pos_avail[tick.btSymbol] > 0 and tick.btSymbol not in context.sellpoint_i_list:
                #在这期间，价格如果下跌到当日涨幅的1%,则卖出
                if tick.lastPrice/tick.preClosePrice - 1 <= context.stoploss_percentage_b and 'i' in context.sellpoint:
                    context.sign_ask = True
                    context.sellpoint_i_list.append(tick.btSymbol)
                    print("%s handle_tick %s" %(STRATEGY_NAME, tick.log_str()), end='\n')
                    print("%s %s %s : %s至收盘下跌到当日%s,卖出" %(STRATEGY_NAME, tick.btSymbol, str(context.sell_time)[:-2], str(context.sell_time)[-2:], context.stoploss_percentage_b), end='\n')
                    context.write_log("{} {} {} : {}至收盘下跌到当日{} ,卖出 {}" .format(STRATEGY_NAME, tick.btSymbol, str(context.sell_time)[:-2], str(context.sell_time)[-2:], context.stoploss_percentage_b, str(-1 * context.pos_avail[tick.btSymbol])))

            #X点的时候价格在涨停板上，X点到收盘的过程中破板，则立即卖出
            elif tick.btSymbol in context.record and context.pos_avail[tick.btSymbol] > 0 and tick.btSymbol not in context.sellpoint_k_list:
                #在这期间，如果该股票破板,则卖出
                if tick.lastPrice < tick.upperLimit and 'k' in context.sellpoint:
                    context.sign_ask = True
                    context.sellpoint_k_list.append(tick.btSymbol)
                    print("%s handle_tick %s" %(STRATEGY_NAME, tick.log_str()), end='\n')
                    print("%s %s %s : %s至收盘过程中破板，卖出" %(STRATEGY_NAME, tick.btSymbol, str(context.sell_time)[:-2], str(context.sell_time)[-2:]), end='\n')
                    context.write_log("{} {} {} : {}至收盘过程中破板{} ,卖出 {}".format(STRATEGY_NAME, tick.btSymbol,str(context.sell_time)[:-2],str(context.sell_time)[-2:],str(-1 * context.pos_avail[tick.btSymbol])))

            # X点的时候价格在涨停板上，X点到X点的过程中破板，则做上记号，准备在指定时间点卖出
            elif tick.btSymbol in context.record and context.pos_avail[tick.btSymbol] > 0:
                if tick.lastPrice < tick.upperLimit and tick.btSymbol not in context.break_sign.keys() and 'l' in context.sellpoint:
                    context.break_sign[tick.btSymbol] = True

    # 到了指定时间点，卖出指定范围的票
    if time_now == context.break_sell_time:
        if tick.btSymbol in context.pos_avail:
            if tick.btSymbol in context.record and context.pos_avail[tick.btSymbol] > 0 and tick.btSymbol not in context.sellpoint_l_list:
                if tick.btSymbol in context.break_sign.keys() and 'l' in context.sellpoint:
                    context.sign_ask = True
                    context.sellpoint_l_list.append(tick.btSymbol)

    #时间到了14:57分
    if time_now == 1457 :
        #当前tick在X点涨停板记录中且当前可卖数量大于0
        if tick.btSymbol in context.record and tick.btSymbol in context.pos_avail and 'h' in context.sellpoint:
            #此时该tick信息没有在涨停板上
            if context.pos_avail[tick.btSymbol] > 0 and tick.btSymbol not in context.sellpoint_h_list:
                if tick.lastPrice < tick.upperLimit:
                    context.sign_ask = True
                    context.sellpoint_h_list.append(tick.btSymbol)
                    print("%s handle_tick %s" %(STRATEGY_NAME, tick.log_str()), end='\n')
                    print("%s  %s : %s在涨停板但尾盘14:57时没在涨停板上，卖出 %s" %(STRATEGY_NAME, str(context.sell_time)[:-2], str(context.sell_time)[-2:], tick.btSymbol), end='\n')
                    context.write_log("{} {} {} : {}在涨停板但尾盘14:57时没在涨停板上，卖出 {}".format(STRATEGY_NAME, tick.btSymbol,  str(context.sell_time)[:-2], str(context.sell_time)[-2:], str(-1 * context.pos_avail[tick.btSymbol])))
        #尾盘14:57分统计9%购买方式购入个股信息
        if tick.btSymbol in context.buy_set and tick.highPrice == tick.upperLimit:
                context.buy_set.remove(tick.btSymbol)

    #记录上笔tick的市价,用于判断下一次tick行情趋势
    if tick.lastPrice != 0:
        context.lastprice[tick.btSymbol] = tick.lastPrice

    #如果该笔tick判断应该买入数量大于0，且实际活动不同个股买单不超过2，同时可追加买单(不同tick行情触发)，发出总order量
    if amount_bid > 0 and (len(set([order.btSymbol for order in context.get_orders(None) if order.direction == Direction.LONG  and order.btSymbol in context.today_symbols])) < context.upperlimit_amount or tick.btSymbol in set([order.btSymbol for order in context.get_orders(None) if order.direction == Direction.LONG  and order.btSymbol in context.today_symbols])):
        position = context.get_account_position(tick.btSymbol)
        orders = context.get_open_orders(tick.btSymbol)
        unfinished_buyorderVolume = sum([k.totalVolume - k.tradedVolume for k in orders if k.direction == Direction.LONG])
        #控制买入量在两层以内(考虑已包含已有持仓)
        amount_bid_adjust = min(amount_bid, min(context.amount_perbuy * context.cash_perbuy, context.amount_limit) / tick.upperLimit - (unfinished_buyorderVolume + position.long_pos))
        amount_bid_adjust_int = int(amount_bid_adjust/100)*100
        #当前股票所发购买委托单数量未超过仓位两层允许购买的数量
        if amount_bid_adjust >= 100 and amount_bid_adjust_int + (position.long_pos - position.long_avail) < context.amount_perbuy * context.cash_perbuy / tick.upperLimit:
            print("%s 账户可用余额 %s" %(STRATEGY_NAME, my_account.account.available))
            #下单总额控制在IMS测试验证具有相关限制，故这里不再重复设置
            context.order(tick.btSymbol, amount_bid_adjust_int, tick.upperLimit)
            print("%s %s %s 该笔tick总计实际买单 %s" %(STRATEGY_NAME, tick.time, tick.btSymbol, amount_bid_adjust_int), end='\n')
            context.write_log('{} {} {} 该笔tick总计实际买单:{} 下单单位价格{} 账户可用余额: {}'.format(STRATEGY_NAME, tick.time, tick.btSymbol, amount_bid_adjust_int, tick.upperLimit, my_account.account.available))

    if context.sign_ask == True  :
        context.order(tick.btSymbol, -1 * context.pos_avail[tick.btSymbol], round(max(tick.lastPrice * 0.98, tick.lowerLimit), 2))
        print("%s %s %s 该笔tick总计卖单 %s" % (STRATEGY_NAME, tick.time, tick.btSymbol, -1 * context.pos_avail[tick.btSymbol]), end='\n')
        context.write_log('{} {} {} 该笔tick总计卖单:{}'.format(STRATEGY_NAME, tick.time, tick.btSymbol, -1 * context.pos_avail[tick.btSymbol]))
        context.sign_ask = False

def handle_bar(context=CTAStrategyEx(None, None), bar=BtBarData()):
    """收到Bar推送"""
    pass

def handle_order(context=CTAStrategyEx(None, None), order=BtOrderData()):
    """收到委托变化推送"""
    # 对于无需做细粒度委托控制的策略，可以忽略
    print("%s handle_order:%s" % (STRATEGY_NAME, order.log_str()), end ='\n')
    context.write_log('{} handle_order {}'.format(STRATEGY_NAME, order.log_str()))
    # 记录账户所持股票可卖数量信息
    context.pos_avail = {k.btSymbol: k.long_avail for k in context.get_account_position(None)}
    if order.direction == Direction.LONG:
        print("%s %s %s已发出买单！" % (STRATEGY_NAME, time.strftime("%H:%M:%S", time.localtime()), order.btSymbol), end='\n')
        context.write_log('{} {} {} 已发出买单！'.format(STRATEGY_NAME, time.strftime("%H:%M:%S", time.localtime()), order.btSymbol))
    elif order.direction == Direction.SHORT:
        print("%s %s %s已发出卖单！" % (STRATEGY_NAME, time.strftime("%H:%M:%S", time.localtime()), order.btSymbol), end='\n')
        context.write_log('{} {} {} 已发出卖单！'.format(STRATEGY_NAME, time.strftime("%H:%M:%S", time.localtime()), order.btSymbol))
    if order.btSymbol in context.cancel_sell_orders:
        if order.status == OrderStatus.CANCELLED or order.status == OrderStatus.PARTCANCELLED:
            context.order(order.btSymbol, -1 * context.cancel_sell_orders[order.btSymbol][0], round(context.cancel_sell_orders[order.btSymbol][1], 2))
            print("%s %s %s已发出卖单追单！" % (STRATEGY_NAME, time.strftime("%H:%M:%S", time.localtime()), order.btSymbol), end='\n')
            context.write_log('{} {} {} 已发出卖单追单！'.format(STRATEGY_NAME, time.strftime("%H:%M:%S", time.localtime()), order.btSymbol))
            del context.cancel_sell_orders[order.btSymbol]

def handle_trade(context=CTAStrategyEx(None, None), trade=BtTradeData()):
    """收到成交推送"""
    print("%s %s handle_trade: %s" %(STRATEGY_NAME, trade.tradeTime, trade.log_str()), end='\n')
    context.write_log('{} {} {} 已成交！'.format(STRATEGY_NAME, time.strftime("%H:%M:%S", time.localtime()), trade.btSymbol))
    context.write_log("{} {} {} 当前持仓 {}".format(STRATEGY_NAME, time.strftime("%H:%M:%S", time.localtime()), trade.btSymbol, context.get_account_position(None)))
    print("%s %s %s已成交！" % (STRATEGY_NAME, trade.tradeTime, trade.btSymbol), end='\n')
    # #TODO 监测实际成交达到数量限制时，撤掉该策略所有买单(后续可能需要该功能)
    # #当成交时监测当前策略已成交股票数量
    # if len([k.btSymbol for k in context.get_account_position(None) if k.btSymbol in context.today_symbols and k.long_avail == 0 and k.long_pos > 0]) >= context.upperlimit_amount:
    #     #若大于等于2，监测当前策略活动买单
    #     if len([order.btSymbol for order in context.get_open_orders(None) if order.direction == Direction.LONG  and order.btSymbol in context.today_symbols]) != 0:
    #         unfinished_open_orders = list(set([order.btSymbol for order in context.get_open_orders(None) if order.direction == Direction.LONG  and order.btSymbol in context.today_symbols and order.tradedVolume != order.totalVolume ]))
    #         for btSymbol in unfinished_open_orders:
    #             #撤掉不同的个股的活动委托买单
    #             context.cancel_order(btSymbol)
